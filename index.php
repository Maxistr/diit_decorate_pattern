<?php
require_once 'TreeFactory.php';
require_once 'TreeSimpleInfo.php';
require_once 'TypeInfoTreeSimpleDecorator.php';
require_once 'AgeInfoTreeSimpleDecorator.php';

$treeFactory = new TreeFactory();

$spruce = $treeFactory->createObject('Ель', 10, 300, 'хвойные деревья');

$simpleInfo = new TreeSimpleInfo();

echo $simpleInfo->writeInfo($spruce->getInfo()) . "\n";

// декорируем по типу
$decorateType = new TypeInfoTreeSimpleDecorator($spruce);
$decorateTypeArr = $decorateType->getInfo();

echo ($simpleInfo->writeInfo($decorateTypeArr)) . "\n";

// декорируем по возрасту
$decorateAge= new AgeInfoTreeSimpleDecorator($spruce);
$decorateAgeArr = $decorateAge->getInfo();

echo ($simpleInfo->writeInfo($decorateAgeArr)) . "\n";

// используем оба декоратора
$decorateTypeMix = new TypeInfoTreeSimpleDecorator($spruce);
$decorateAgeMix= new AgeInfoTreeSimpleDecorator($decorateTypeMix);
$decorateMixArr = $decorateAgeMix->getInfo();

echo ($simpleInfo->writeInfo($decorateMixArr)) . "\n";

?>
