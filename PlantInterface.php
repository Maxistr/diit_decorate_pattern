<?php

interface PlantInterface
{

    /**
     * Метод который возвращает набор основных параметров растений
     * @return string
     */
    public function getInfo():array;

}