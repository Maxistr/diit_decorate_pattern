<?php
require_once 'Tree.php';

class TreeFactory {

    public function createObject($name, $age, $height, $type = Tree::NONE):Tree
    {
        $tree = new Tree();

        $tree->setName($name);
        $tree->setAge($age);
        $tree->setHeight($height);
        $tree->setType($type);

        return $tree;
    }
}