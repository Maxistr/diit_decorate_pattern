<?php
require_once 'Tree.php';
require_once 'TreeInfoInterface.php';

class TreeSimpleInfo implements TreeInfoInterface
{
    public function writeInfo($data)
    {
        if (!isset($data['name'], $data['age'], $data['height'], $data['type'])) {
            throw new Exception('Do not set data params');
        }
        $info = "Название дерева: {$data['name']}. Его возраст: " .
            "{$data['age']} лет. Имеет высоту: {$data['height']} метров.";
        if ($data['type'] !== Tree::NONE) {
            $info = $info . " Относиться к классу: {$data['type']}.";
        }
        return $info . "\n";
    }
}