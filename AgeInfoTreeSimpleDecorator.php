<?php
require_once 'InfoDecoratorAbstract.php';
require_once 'Tree.php';

class AgeInfoTreeSimpleDecorator extends InfoDecoratorAbstract
{

    public function getInfo():array
    {
        $arr = $this->tree->getInfo();
        if (!isset($arr['age'])) {
            throw new Exception('Does not set age');
        }

        if ($arr['age'] < 20) {
            $text = '(Это достаточно молодое дерево)';
        } else {
            $text = '(Это дерево имеет свою историю)';
        }

        $result = $arr;
        $result['age'] = $text . ' ' . $arr['age'];

        return $result;
    }
}