<?php

require_once 'TreeInfoInterface.php';
require_once 'Tree.php';

abstract class InfoDecoratorAbstract implements PlantInterface
{
    protected $tree;

    public function __construct(PlantInterface $input)
    {
        $this->tree = $input;
    }

    abstract public function getInfo():array;
}



