<?php
require_once 'InfoDecoratorAbstract.php';
require_once 'Tree.php';

class TypeInfoTreeSimpleDecorator extends InfoDecoratorAbstract
{
    const TREE_TYPE_INFO = [
        'хвойные деревья' => 'Это дерево не привередливо к условиям в которых оно растет',
        'лиственные деревья' => 'Это дерево требует умерянный климат',
        'плодовые деревья' => 'Это дерево может давать съедобные плоды',
    ];

    public function getInfo():array
    {
        $arr = $this->tree->getInfo();
        if (!isset($arr['type'])) {
            throw new Exception('Does not set type');
        }

        $text = '';

        if ($arr['type'] !== Tree::NONE) {
            $text = self::TREE_TYPE_INFO[$arr['type']];
        }

        $result = $arr;
        $result['type'] = $arr['type'] . '. ' . $text;

        return $result;
    }
}